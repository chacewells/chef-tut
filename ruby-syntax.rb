puts "yay"
foo = "bar"
puts foo

variable = 2
a = 1
b = 'hello'
c = Object.new

bacon_type = 'crispy'

2.times do
  puts bacon_type
  temperature = 300
end

begin
    puts temperature
rescue Exception => e
    puts "\"#{e.message}\" is a problem. what do you think?"
end

hour = 6
if hour % 3 == 0
  puts "It's BACON time!"
end

"double quoted string"
'single quoted string'

# double-quoted strings can interpolate variables
x = "hello"
puts "#{x} world"
puts '#{x} world'

# also, escape quotes with \ as in other languages
puts "quotes in \"quotes\""
puts 'quotes in \'quotes\''

interpolation = "THE INTERPOLATION FEATURE"

puts <<METHOD_DESCRIPTION
This is a multiline string.

All the whitespace is preserved, and I can even apply #{interpolation} inside
this block.
METHOD_DESCRIPTION

types = [ 'crispy', 'raw', 'crunchy', 'grilled' ]
puts "length: #{types.length}"
puts "size: #{types.size}"
puts "#{types}"
puts

types.push 'smoked'
puts "#{types}"
puts

types << 'deep fried'
puts "#{types}"
puts

puts "#{types[0]}"
puts types.first
puts types.last
puts "#{types[0..1]}"
